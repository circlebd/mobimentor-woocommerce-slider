<?php
global $product;
$id = $product->get_id();
$thumbnail_id = $product->get_image_id();
//$thimbnail_size = $settings['thumbnail_size_size'] ? $settings['thumbnail_size_size'] : 'woocommerce_single';


$image_src = \Elementor\Group_Control_Image_Size::get_attachment_image_src($thumbnail_id, 'thumbnail_size', $settings);
if ($image_src) :
    $image_size = mobimentor_image_ratio($image_src, $settings);
    $height = $settings['carousel_height'] ? $settings['carousel_height'] : 200;
    $content_ratio = $settings['content_ratio']['size'] ? $settings['content_ratio']['size'] : 50;
    $content_margin = $settings['content_margin'] ? $settings['content_margin'] : 10;
    $slider_margin = $settings['slider_margin'] ? $settings['slider_margin'] : 10;
    $label_size = ceil((10 / 50) * $content_ratio);
    $price_size = ceil((14 / 50) * $content_ratio);
    $add_to_cart = do_shortcode('[add_to_cart_url id="' . $id . '"]');

    $categories = get_the_terms($id, 'product_cat');
    $product_cat = '';
    foreach ($categories as $category) {
        $product_cat = $category->name;
        break;
    }

    $show_price = $settings['show_price'] == 'yes' ? '' : 'price-hidden';
    $show_reg_price = $settings['show_regular_price'] == 'yes' ? '' : 'reg-price-hidden';
    $slide_background = $settings['display_background_blur'] == 'yes' ? '' : 'background-image: unset';
?>
    <div>
        <div class="single-slider" style="background: url(<?php echo $image_src; ?>) center center / cover no-repeat; height: <?php echo $height; ?>px; margin-right: <?php echo $slider_margin; ?>px;">
            <div class="slider-foreground" style="<?php echo $slide_background; ?>">
                <div class="slide-top-left" style="<?php echo 'margin:' . $content_margin . 'px'; ?>">
                    <?php
                    if ($settings['show_onsale'] == 'yes') :
                    ?>
                        <span class="on-sale" style="<?php echo 'font-size:' . $label_size . 'px'; ?>">On sale</span>
                    <?php
                    endif;
                    ?>

                    <?php
                    if ($settings['show_featured'] == 'yes') :
                    ?>
                        <span class="featured" style="<?php echo 'font-size:' . $label_size . 'px'; ?>">Featured</span>
                    <?php
                    endif;
                    ?>
                </div>
                <div class="slide-top-right" style="<?php echo 'margin:' . $content_margin . 'px'; ?>">
                    <?php //echo do_shortcode('[yith_wcwl_add_to_wishlist]'); 
                    ?>
                    <?php
                    if ($settings['show_addtocart'] == 'yes') :
                        echo sprintf(
                            '<a href="%s" data-quantity="1" class="%s" %s style="font-size:' . $price_size . 'px" title="%s">%s</a>',
                            esc_url($product->add_to_cart_url()),
                            esc_attr(implode(' ', array_filter(array(
                                'product_type_' . $product->get_type(),
                                $product->is_purchasable() && $product->is_in_stock() ? 'add_to_cart_button' : '',
                                $product->supports('ajax_add_to_cart') ? 'ajax_add_to_cart' : '',
                                'mm-add-to-cart-btn'
                            )))),
                            wc_implode_html_attributes(array(
                                'data-product_id'  => $product->get_id(),
                                'data-product_sku' => $product->get_sku(),
                                'aria-label'       => $product->add_to_cart_description(),
                                'rel'              => 'nofollow',
                            )),
                            'Add to cart',
                            '<span class="fa-stack fa-2x">
                                <i class="fas fa-circle fa-stack-2x"></i>
                                <i class="fas fa-cart-plus fa-stack-1x fa-inverse"></i>
                            </span>'
                            //'<i class="fas fa-cart-plus"></i>'
                        );
                    endif;
                    ?>
                </div>

            </div>
            <?php
            //echo sprintf('<img src="%s" title="%s" %s style="%s"/>', esc_attr($image_src), get_the_title($thumbnail_id), '', $image_size);
            ?>
        </div>

        <div class="slider-information">
            <div class="slider-info" style="<?php echo 'margin:' . $content_margin . 'px'; ?>">
                <?php
                if ($product_cat && $settings['show_category'] == 'yes') :
                ?>
                    <span class="product-category" style="<?php echo 'font-size:' . $label_size . 'px'; ?>">
                        <?php
                        echo $product_cat;
                        ?>
                    </span>
                <?php
                endif;
                ?>
                <a href="<?php echo get_permalink($id) ?>" class="slider-link" target="_blank">
                    <!-- <h4 class="product-title" style="<?php echo 'font-size:' . $price_size . 'px'; ?>"><?php echo $product->get_name(); ?></h4> -->
                    <h4 class="product-title"><?php echo $product->get_name(); ?></h4>
                    <div class="product-price <?php echo $show_price . ' ' . $show_reg_price; ?>" style="<?php echo 'font-size:' . $price_size . 'px'; ?>"><?php echo $product->get_price_html(); ?></div>
                </a>
            </div>
        </div>
    </div>
<?php
endif;
