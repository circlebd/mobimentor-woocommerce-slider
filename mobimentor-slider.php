<?php

/**
 * 
 * Plugin Name: Mobimentor Slider - An Elementor Slider for WooCommerce Product
 * Plugin URI: http://mobimentor.net
 * Description: An elementor addon specialized for Mobile Sites. User can create woocommerce product slider and carousel nicely.
 * Version: 1.0.0
 * Author: Mahfuzul Alam
 * Author URI: https://www.linkedin.com/in/mahfuzul-alam/
 * Text Domain: mobimentor-slider
 * Domain Path: /languages/
 * Requires at least: 5.6
 * Requires PHP: 7.0
 * License: GPL2
 * 
 */
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

/**
 * Mobimentor Slider Plugin Core Class
 *
 * The main class that initiates and runs the plugin.
 *
 * @since 1.0.0
 */
final class MobimentorSlider
{
    /**
     * Plugin Version
     *
     * @since 1.0.0
     *
     * @var string The plugin version.
     */
    const VERSION = '1.0.0';

    /**
     * Minimum Elementor Version
     *
     * @since 1.0.0
     *
     * @var string Minimum Elementor version required to run the plugin.
     */
    const MINIMUM_ELEMENTOR_VERSION = '2.0.0';

    /**
     * Minimum PHP Version
     *
     * @since 1.0.0
     *
     * @var string Minimum PHP version required to run the plugin.
     */
    const MINIMUM_PHP_VERSION = '7.0';

    /**
     * Instance
     *
     * @since 1.0.0
     *
     * @access private
     * @static
     *
     * @var MobimentorSlider The single instance of the class.
     */
    private static $_instance = null;

    /**
     * Instance
     *
     * Ensures only one instance of the class is loaded or can be loaded.
     *
     * @since 1.0.0
     *
     * @access public
     * @static
     *
     * @return MobimentorSlider An instance of the class.
     */
    public static function instance()
    {

        if (is_null(self::$_instance)) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    /**
     * Constructor
     *
     * @since 1.0.0
     *
     * @access public
     */
    public function __construct()
    {
        add_action('plugins_loaded', [$this, 'on_plugins_loaded']);
    }

    /**
     * Load Textdomain
     *
     * Load plugin localization files.
     *
     * Fired by `init` action hook.
     *
     * @since 1.0.0
     *
     * @access public
     */
    public function i18n()
    {
        load_plugin_textdomain('mobimentor-slider');
    }

    /**
     * On Plugins Loaded
     *
     * Checks if Elementor has loaded, and performs some compatibility checks.
     * If All checks pass, inits the plugin.
     *
     * Fired by `plugins_loaded` action hook.
     *
     * @since 1.0.0
     *
     * @access public
     */
    public function on_plugins_loaded()
    {

        if ($this->is_compatible()) {
            add_action('elementor/init', [$this, 'init']);
            // Add a custom category for panel widgets
            add_action('elementor/init', [$this, 'mm_create_category']);
            add_action('wp_enqueue_scripts', function () {
                if ($this->is_elementor() == 'builder') {
                    wp_register_script('slick-js', plugin_dir_url(__FILE__) . '/assets/vendors/slick/slick.min.js');
                    wp_register_script('mobimentor-slider-js', plugin_dir_url(__FILE__) . '/dist/js/mobimentor-slider.min.js?v=' . time());
                    wp_enqueue_script('slick-js');
                    wp_enqueue_script('mobimentor-slider-js');
                }
            });
        }
    }

    public function enqueue_styles()
    {
        wp_enqueue_style('slick-style', plugin_dir_url(__FILE__) . '/assets/vendors/slick/slick.css', '1.0.0', 'all');
        wp_enqueue_style('mobimentor-slider-style', plugin_dir_url(__FILE__) . '/dist/css/style.css?v=' . time(), array('slick-style'));
        /* wp_enqueue_style('slick-style');
        wp_enqueue_style('mobimentor-slider-style'); */
    }

    /**
     * Compatibility Checks
     *
     * Checks if the installed version of Elementor meets the plugin's minimum requirement.
     * Checks if the installed PHP version meets the plugin's minimum requirement.
     *
     * @since 1.0.0
     *
     * @access public
     */
    public function is_compatible()
    {

        // Check if Elementor installed and activated
        if (!did_action('elementor/loaded')) {
            add_action('admin_notices', [$this, 'admin_notice_missing_main_plugin']);
            return false;
        }

        // Check for required Elementor version
        if (!version_compare(ELEMENTOR_VERSION, self::MINIMUM_ELEMENTOR_VERSION, '>=')) {
            add_action('admin_notices', [$this, 'admin_notice_minimum_elementor_version']);
            return false;
        }

        // Check for required PHP version
        if (version_compare(PHP_VERSION, self::MINIMUM_PHP_VERSION, '<')) {
            add_action('admin_notices', [$this, 'admin_notice_minimum_php_version']);
            return false;
        }

        return true;
    }

    /**
     * Initialize the plugin
     *
     * Load the plugin only after Elementor (and other plugins) are loaded.
     * Load the files required to run the plugin.
     *
     * Fired by `plugins_loaded` action hook.
     *
     * @since 1.0.0
     *
     * @access public
     */
    public function init()
    {

        $this->i18n();
        $this->defined_constants();
        $this->include_files();
        // Add Plugin actions
        add_action('elementor/widgets/widgets_registered', [$this, 'init_widgets']);
    }

    public function include_files()
    {
        include_once(MMS_IS_PATH . 'inc/functions.php');
        add_action('elementor/frontend/after_enqueue_styles', [$this, 'enqueue_styles']);
    }


    public function defined_constants()
    {
        define('MMS_IS_PATH', plugin_dir_path(__FILE__));
        define('MMS_IS_URL', plugin_dir_url(__FILE__));
        define('MMS_IS_VERSION', '1.0.0'); //Plugin Version
        define('MMS_IS_MIN_ELEMENTOR_VERSION', '2.0.0'); //MINIMUM ELEMENTOR VERSION
        define('MMS_IS_MIN_PHP_VERSION', '5.4'); //MINIMUM PHP VERSION
    }

    /**
     * Init Widgets
     *
     * Include widgets files and register them
     *
     * @since 1.0.0
     *
     * @access public
     */
    public function init_widgets()
    {
        // Include Widget files
        require_once(__DIR__ . '/widgets/woocommerce-product-carousel.php');

        // Register widget
        \Elementor\Plugin::instance()->widgets_manager->register_widget_type(new \Mobimentor_WC_Product_Carousel());
    }

    /**
     * Init Controls
     *
     * Include controls files and register them
     *
     * @since 1.0.0
     *
     * @access public
     */
    public function init_controls()
    {
    }

    /**
     * Init Custom Category
     *
     * Elementor new category register method
     *
     * @since 1.0.0
     *
     * @access public
     */
    public function mm_create_category()
    {
        \Elementor\Plugin::$instance->elements_manager->add_category(
            'mobimentor',
            [
                'title' => esc_html('Mobimentor', 'mobimentor-slider'),
                'icon' => 'fa fa-plug', //default icon
            ],
            3 // position
        );
    }

    /**
     * Admin notice
     *
     * Warning when the site doesn't have Elementor installed or activated.
     *
     * @since 1.0.0
     *
     * @access public
     */
    public function admin_notice_missing_main_plugin()
    {
        if (isset($_GET['activate'])) unset($_GET['activate']);

        $message = sprintf(
            /* translators: 1: Plugin name 2: Elementor */
            esc_html__('"%1$s" requires "%2$s" to be installed and activated.', 'mobimentor-slider'),
            '<strong>' . esc_html__('Elementor Test Extension', 'mobimentor-slider') . '</strong>',
            '<strong>' . esc_html__('Elementor', 'mobimentor-slider') . '</strong>'
        );

        printf('<div class="notice notice-warning is-dismissible"><p>%1$s</p></div>', $message);
    }

    /**
     * Admin notice
     *
     * Warning when the site doesn't have a minimum required Elementor version.
     *
     * @since 1.0.0
     *
     * @access public
     */
    public function admin_notice_minimum_elementor_version()
    {

        if (isset($_GET['activate'])) unset($_GET['activate']);

        $message = sprintf(
            /* translators: 1: Plugin name 2: Elementor 3: Required Elementor version */
            esc_html__('"%1$s" requires "%2$s" version %3$s or greater.', 'mobimentor-slider'),
            '<strong>' . esc_html__('Elementor Test Extension', 'mobimentor-slider') . '</strong>',
            '<strong>' . esc_html__('Elementor', 'mobimentor-slider') . '</strong>',
            self::MINIMUM_ELEMENTOR_VERSION
        );

        printf('<div class="notice notice-warning is-dismissible"><p>%1$s</p></div>', $message);
    }

    /**
     * Admin notice
     *
     * Warning when the site doesn't have a minimum required PHP version.
     *
     * @since 1.0.0
     *
     * @access public
     */
    public function admin_notice_minimum_php_version()
    {

        if (isset($_GET['activate'])) unset($_GET['activate']);

        $message = sprintf(
            /* translators: 1: Plugin name 2: PHP 3: Required PHP version */
            esc_html__('"%1$s" requires "%2$s" version %3$s or greater.', 'mobimentor-slider'),
            '<strong>' . esc_html__('Elementor Test Extension', 'mobimentor-slider') . '</strong>',
            '<strong>' . esc_html__('PHP', 'mobimentor-slider') . '</strong>',
            self::MINIMUM_PHP_VERSION
        );

        printf('<div class="notice notice-warning is-dismissible"><p>%1$s</p></div>', $message);
    }

    /**
     * Check if page is an elementor page
     *
     * Return if the page is elementor page or not
     *
     * @since 1.0.0
     *
     * @access public
     */
    public function is_elementor()
    {
        global $post;
        return get_post_meta(get_the_ID(), '_elementor_edit_mode', true);
    }
}

MobimentorSlider::instance();
