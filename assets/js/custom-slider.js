jQuery(document).ready(function($){

    $('.mobimentor-slick-slider').each(function(index, slider){
        var slides = $(this).attr('data-slide')
        $(this).slick({
            autoplay: false,
            slidesToShow: slides,
            slidesToScroll: 1,
            variableWidth: false,
            prevArrow: '<div class="slick-arrow-left"><i class="fa fa-chevron-left" aria-hidden="true"></i></div>',
            nextArrow: '<div class="slick-arrow-right"><i class="fa fa-chevron-right" aria-hidden="true"></i></div>',
            dots: false,
            responsive: [
                {
                breakpoint: 769,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '40px',
                    slidesToShow: 2
                }
                },
                {
                breakpoint: 482,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '40px',
                    slidesToShow: 1
                }
                }
            ]
        });
    })
    
})

