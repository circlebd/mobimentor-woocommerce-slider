<?php

/**
 * Mobimentor - Woocommerce Product Carousel.
 *
 * Elementor widget that displays an woocommerce product carousel.
 *
 * @since 1.0.0
 */
class Mobimentor_WC_Product_Carousel extends \Elementor\Widget_Base
{

    /**
     * Get widget name.
     *
     * Retrieve WooCommerce Product Carousel widget name.
     *
     * @since 1.0.0
     * @access public
     *
     * @return string WooCommerce Product Carousel.
     */
    public function get_name()
    {
        return 'modimentor_woocommerce_product_carousel';
    }

    /**
     * Get widget title.
     *
     * Retrieve WooCommerce Product Carousel widget title.
     *
     * @since 1.0.0
     * @access public
     *
     * @return string Widget title.
     */
    public function get_title()
    {
        return __('WooCommerce Product Carousel', 'mobimentor-slider');
    }

    /**
     * Get widget icon.
     *
     * Retrieve WooCommerce Product Carousel widget icon.
     *
     * @since 1.0.0
     * @access public
     *
     * @return string Widget icon.
     */
    public function get_icon()
    {
        return 'eicon-slider-push';
    }

    /**
     * Get widget categories.
     *
     * Retrieve the list of categories the WooCommerce Product Carousel widget belongs to.
     *
     * @since 1.0.0
     * @access public
     *
     * @return array Widget categories.
     */
    public function get_categories()
    {
        //return ['woocommerce-elements', 'mobimentor'];
        return ['mobimentor'];
    }

    /**
     * Register WooCommerce product carousel widget controls.
     *
     * Adds different input fields to allow the user to change and customize the widget settings.
     *
     * @since 1.0.0
     * @access protected
     */
    protected function _register_controls()
    {

        $this->start_controls_section(
            'title_section',
            [
                'label' => esc_html__('Title Settings', 'mobimentor-slider'),
                'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
            ]
        );

        $this->add_control(
            'display_title',
            [
                'label' => esc_html__('Show Title', 'mobimentor-slider'),
                'type' => \Elementor\Controls_Manager::SWITCHER,
                'label_on' => esc_html__('Yes', 'mobimentor-slider'),
                'label_off' => esc_html__('No', 'mobimentor-slider'),
                'return_value' => 'yes',
                'default' => 'yes',
            ]
        );

        $this->add_control(
            'title',
            [
                'label' => esc_html__('Title', 'mobimentor-slider'),
                'type' => \Elementor\Controls_Manager::TEXT,
                'input_type' => 'text',
                'placeholder' => esc_html__('Enter the title', 'mobimentor-slider'),
                'condition' => [
                    'display_title'    =>    'yes',
                ]
            ]
        );

        $this->add_control(
            'display_subtitle',
            [
                'label' => esc_html__('Show Subtitle', 'mobimentor-slider'),
                'type' => \Elementor\Controls_Manager::SWITCHER,
                'label_on' => esc_html__('Yes', 'mobimentor-slider'),
                'label_off' => esc_html__('No', 'mobimemobimentor-sliderntor'),
                'return_value' => 'yes',
                'default' => 'yes',
            ]
        );

        $this->add_control(
            'subtitle',
            [
                'label' => esc_html__('Subtitle', 'mobimentor-slider'),
                'type' => \Elementor\Controls_Manager::TEXT,
                'input_type' => 'text',
                'placeholder' => esc_html__('Enter the subtitle', 'mobimentor-slider'),
                'condition' => [
                    'display_subtitle'    =>    'yes',
                ]
            ]
        );

        $this->add_control(
            'show_more',
            [
                'label' => esc_html__('Enable Show More', 'mobimentor-slider'),
                'type' => \Elementor\Controls_Manager::SWITCHER,
                'label_on' => esc_html__('Yes', 'mobimentor-slider'),
                'label_off' => esc_html__('No', 'mobimentor-slider'),
                'return_value' => 'yes',
                'default' => 'yes',
            ]
        );

        $this->add_control(
            'show_more_text',
            [
                'label' => esc_html__('Show More Text', 'mobimentor-slider'),
                'type' => \Elementor\Controls_Manager::TEXT,
                'input_type' => 'text',
                'placeholder' => esc_html__('Enter show more text', 'mobimentor-slider'),
                'default' => 'Show More',
                'condition' => [
                    'show_more'    =>    'yes',
                ]
            ]
        );

        $this->add_control(
            'show_more_link',
            [
                'label' => esc_html__('Show More Link', 'mobimentor-slider'),
                'type' => \Elementor\Controls_Manager::TEXT,
                'input_type' => 'text',
                'placeholder' => esc_html__('Enter show more link', 'mobimentor-slider'),
                'default' => '/shop/',
                'condition' => [
                    'show_more'    =>    'yes',
                ]
            ]
        );

        $this->add_control(
            'title_style',
            [
                'label' => esc_html__('Title style', 'mobimmobimentor-sliderentor'),
                'type' => \Elementor\Controls_Manager::SELECT,
                'default' => 'title-one',
                'options' => [
                    'title-one'  => esc_html__('Title 1', 'mobimentor-slider'),
                    'title-two' => esc_html__('Title 2', 'mobimentor-slider'),
                    'title-three' => esc_html__('Title 3', 'mobimentor-slider'),
                    'title-four' => esc_html__('Title 4', 'mobimentor-slider'),
                    'title-default' => esc_html__('Default', 'mobimentor-slider'),
                ],
            ]
        );

        $this->end_controls_section();

        $this->start_controls_section(
            'product_section',
            [
                'label' => __('Product Settings', 'mobimentor-slider'),
                'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
            ]
        );

        $this->add_control(
            'product_limit',
            [
                'label' => __('Product limit', 'mobimentor-slider'),
                'type' => \Elementor\Controls_Manager::NUMBER,
                'default' => 10,
                'min'   => -1,
            ]
        );

        $this->add_control(
            'product_status',
            [
                'label' => esc_html__('Product Status', 'mobimentor-slider'),
                'placeholder' => esc_html__('Choose Product Status', 'mobimentor-slider'),
                'type' => \Elementor\Controls_Manager::SELECT2,
                'label_block' => true,
                'default' => 'publish',
                'multiple' => true,
                'options' => mobimentor_get_product_status(),
            ]
        );

        $this->add_control(
            'product_types',
            [
                'label' => esc_html__('Product Types', 'mobimentor-slider'),
                'placeholder' => esc_html__('Choose Product Types', 'mobimentor-slider'),
                'type' => \Elementor\Controls_Manager::SELECT2,
                'label_block' => true,
                'multiple' => true,
                'default' => '',
                'options' => mobimentor_get_product_types(),
            ]
        );

        $this->add_control(
            'product_cats',
            [
                'label' => esc_html__('Product Categories', 'mobimentor-slider'),
                'placeholder' => esc_html__('Choose Product Categories', 'mobimentor-slider'),
                'type' => \Elementor\Controls_Manager::SELECT2,
                'label_block' => true,
                'multiple' => true,
                'default' => '',
                'options' => mobimentor_get_product_cats(),
            ]
        );

        $this->add_control(
            'product_options',
            [
                'label' => __('Product options', 'mobimentor-slider'),
                'type' => \Elementor\Controls_Manager::SELECT,
                'default' => 'all',
                'options' => [
                    'featured'  => __('Featured products', 'mobimentor-slider'),
                    'on-sale' => __('On-sale products', 'mobimentor-slider'),
                    'best-selling' => __('Best selling products', 'mobimentor-slider'),
                    'all' => __('All products', 'mobimentor-slider'),
                ],
            ]
        );

        $this->add_control(
            'product_sorting_by',
            [
                'label' => __('Product sorting by', 'mobimentor-slider'),
                'type' => \Elementor\Controls_Manager::SELECT,
                'default' => 'date',
                'options' => [
                    'date'  => __('Date', 'mobimentor-slider'),
                    'price' => __('Price', 'mobimentor-slider'),
                    'name' => __('Name', 'mobimentor-slider'),
                ],
            ]
        );

        $this->add_control(
            'product_sorting_order',
            [
                'label' => __('Product sorting order', 'mobimentor-slider'),
                'type' => \Elementor\Controls_Manager::SELECT,
                'default' => 'asc',
                'options' => [
                    'asc'  => __('Ascending', 'mobimentor-slider'),
                    'desc' => __('Descending', 'mobimentor-slider'),
                ],
            ]
        );

        $this->end_controls_section();

        $this->start_controls_section(
            'item_section',
            [
                'label' => __('Item Settings', 'mobimentor-slider'),
                'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
            ]
        );

        $this->add_control(
            'template_style',
            [
                'label' => esc_html__('Template style', 'mobimentor-slider'),
                'type' => \Elementor\Controls_Manager::SELECT,
                'default' => 'template-one',
                'options' => [
                    'template-one'  => esc_html__('Template 1', 'mobimentor-slider'),
                    'template-two' => esc_html__('Template 2', 'mobimentor-slider'),
                    'template-three' => esc_html__('Template 3', 'mobimentor-slider'),
                    'template-four' => esc_html__('Template 4', 'mobimentor-slider'),
                    'template-five' => esc_html__('Template 5', 'mobimentor-slider'),
                ],
            ]
        );

        $this->add_control(
            'show_image',
            [
                'label' => esc_html__('Show Image', 'mobimentor-slider'),
                'type' => \Elementor\Controls_Manager::SWITCHER,
                'label_on' => esc_html__('Yes', 'mobimentor-slider'),
                'label_off' => esc_html__('No', 'mobimentor-slider'),
                'return_value' => 'yes',
                'default' => 'yes',
                'condition' => [
                    'template_style' => ['template-two', 'template-three']
                ]
            ]
        );

        $this->add_control(
            'content_ratio',
            [
                'label' => __('Content Ratio', 'plugin-domain'),
                'type' => \Elementor\Controls_Manager::SLIDER,
                'size_units' => ['%'],
                'range' => [
                    '%' => [
                        'min' => 20,
                        'max' => 80,
                    ],
                ],
                'default' => [
                    'unit' => '%',
                    'size' => 50,
                ],
            ]
        );

        $this->add_control(
            'content_margin',
            [
                'label' => __('Content Margin', 'mobimentor-slider'),
                'type' => \Elementor\Controls_Manager::NUMBER,
                'default' => 10,
                'min'   => 0,
                'max'   => 50,
            ]
        );

        $this->add_control(
            'slider_margin',
            [
                'label' => __('Item Margin', 'mobimentor-slider'),
                'type' => \Elementor\Controls_Manager::NUMBER,
                'default' => 10,
                'min'   => 0,
                'max'   => 30,
            ]
        );

        $this->add_group_control(
            \Elementor\Group_Control_Image_Size::get_type(),
            [
                'name' => 'thumbnail_size',
                'default' => 'medium_large',
                'condition' => [
                    'show_image'    =>    'yes',
                ]
            ]
        );

        $this->add_control(
            'show_onsale',
            [
                'label' => esc_html__('On-sale Label', 'mobimentor-slider'),
                'type' => \Elementor\Controls_Manager::SWITCHER,
                'label_on' => esc_html__('Yes', 'mobimentor-slider'),
                'label_off' => esc_html__('No', 'mobimentor-slider'),
                'return_value' => 'yes',
                'default' => 'yes',
            ]
        );

        $this->add_control(
            'show_featured',
            [
                'label' => esc_html__('Featured Label', 'mobimentor-slider'),
                'type' => \Elementor\Controls_Manager::SWITCHER,
                'label_on' => esc_html__('Yes', 'mobimentor-slider'),
                'label_off' => esc_html__('No', 'mobimentor-slider'),
                'return_value' => 'yes',
                'default' => 'yes',
            ]
        );

        $this->add_control(
            'show_price',
            [
                'label' => esc_html__('Price Label', 'mobimentor-slider'),
                'type' => \Elementor\Controls_Manager::SWITCHER,
                'label_on' => esc_html__('Yes', 'mobimentor-slider'),
                'label_off' => esc_html__('No', 'mobimentor-slider'),
                'return_value' => 'yes',
                'default' => 'yes',
            ]
        );

        $this->add_control(
            'show_regular_price',
            [
                'label' => esc_html__('Regular Price Label', 'mobimentor-slider'),
                'type' => \Elementor\Controls_Manager::SWITCHER,
                'label_on' => esc_html__('Yes', 'mobimentor-slider'),
                'label_off' => esc_html__('No', 'mobimentor-slider'),
                'return_value' => 'yes',
                'default' => 'yes',
                'condition' => [
                    'show_price' => 'yes',
                ]
            ]
        );

        $this->add_control(
            'show_addtocart',
            [
                'label' => esc_html__('Add to cart button', 'mobimentor-slider'),
                'type' => \Elementor\Controls_Manager::SWITCHER,
                'label_on' => esc_html__('Yes', 'mobimentor-slider'),
                'label_off' => esc_html__('No', 'mobimentor-slider'),
                'return_value' => 'yes',
                'default' => 'yes',
            ]
        );

        $this->add_control(
            'show_category',
            [
                'label' => esc_html__('Category Label', 'mobimentor-slider'),
                'type' => \Elementor\Controls_Manager::SWITCHER,
                'label_on' => esc_html__('Yes', 'mobimentor-slider'),
                'label_off' => esc_html__('No', 'mobimentor-slider'),
                'return_value' => 'yes',
                'default' => 'yes',
            ]
        );

        $this->end_controls_section();

        $this->start_controls_section(
            'carousel_section',
            [
                'label' => __('Carousel Settings', 'mobimentor-slider'),
                'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
            ]
        );

        $this->add_control(
            'carousel_height',
            [
                'label' => __('Carousel Height', 'mobimentor-slider'),
                'type' => \Elementor\Controls_Manager::NUMBER,
                'default' => 300,
            ]
        );

        $this->add_control(
            'slides_to_show',
            [
                'label' => __('Slides to Show', 'mobimentor-slider'),
                'type' => \Elementor\Controls_Manager::NUMBER,
                'default' => 3,
                'min' => 1,
                'max' => 10,
            ]
        );

        $this->add_control(
            'slides_to_scroll',
            [
                'label' => __('Slides to Scroll', 'mobimentor-slider'),
                'type' => \Elementor\Controls_Manager::NUMBER,
                'default' => 3,
                'min' => 1,
                'max' => 10,
            ]
        );


        $this->add_control(
            'pause_on_focus',
            [
                'label' => esc_html__('Pause On Focus', 'mobimentor-slider'),
                'type' => \Elementor\Controls_Manager::SWITCHER,
                'label_on' => esc_html__('Yes', 'mobimentor-slider'),
                'label_off' => esc_html__('No', 'mobimentor-slider'),
                'return_value' => 'yes',
                'default' => 'yes',
            ]
        );

        $this->add_control(
            'display_nav_arrows',
            [
                'label' => esc_html__('Display Navigation Arrows', 'mobimentor-slider'),
                'type' => \Elementor\Controls_Manager::SWITCHER,
                'label_on' => esc_html__('Yes', 'mobimentor-slider'),
                'label_off' => esc_html__('No', 'mobimentor-slider'),
                'return_value' => 'yes',
                'default' => 'yes',
            ]
        );

        $this->add_control(
            'display_dots',
            [
                'label' => esc_html__('Display Dots', 'mobimentor-slider'),
                'type' => \Elementor\Controls_Manager::SWITCHER,
                'label_on' => esc_html__('Yes', 'mobimentor-slider'),
                'label_off' => esc_html__('No', 'mobimentor-slider'),
                'return_value' => 'yes',
                'default' => 'yes',
            ]
        );

        $this->add_control(
            'autoplay',
            [
                'label' => esc_html__('AutoPlay', 'mobimentor-slider'),
                'type' => \Elementor\Controls_Manager::SWITCHER,
                'label_on' => esc_html__('Yes', 'mobimentor-slider'),
                'label_off' => esc_html__('No', 'mobimentor-slider'),
                'return_value' => 'yes',
                'default' => 'yes',
            ]
        );

        $this->add_control(
            'autoplay_speed',
            [
                'label' => esc_html__('AutoPlay Speed', 'mobimentor-slider'),
                'type' => \Elementor\Controls_Manager::NUMBER,
                'default' => 5000,
                'condition' => [
                    'autoplay' => 'yes',
                ]
            ]
        );

        $this->add_control(
            'pause_on_hover',
            [
                'label' => esc_html__('Pause On Hover', 'mobimentor-slider'),
                'type' => \Elementor\Controls_Manager::SWITCHER,
                'label_on' => esc_html__('Yes', 'mobimentor-slider'),
                'label_off' => esc_html__('No', 'mobimentor-slider'),
                'return_value' => 'yes',
                'default' => 'yes',
            ]
        );

        $this->add_control(
            'pause_dots_hover',
            [
                'label' => esc_html__('Pause On Dots Hover', 'mobimentor-slider'),
                'type' => \Elementor\Controls_Manager::SWITCHER,
                'label_on' => esc_html__('Yes', 'mobimentor-slider'),
                'label_off' => esc_html__('No', 'mobimentor-slider'),
                'return_value' => 'yes',
                'default' => 'yes',
            ]
        );

        $this->add_control(
            'slide_speed',
            [
                'label' => esc_html__('Slide Speed', 'mobimentor-slider'),
                'type' => \Elementor\Controls_Manager::NUMBER,
                'default' => 5000,
            ]
        );

        $this->end_controls_section();

        $this->start_controls_section(
            'responsive_section',
            [
                'label' => __('Responsive Settings', 'mobimentor-slider'),
                'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
            ]
        );

        /* $this->add_control(
            'title_font_size_mobile',
            [
                'label' => __('Title Font Size', 'mobimentor-slider'),
                'type' => \Elementor\Controls_Manager::NUMBER,
                'default' => 16,
                'min'   => 1,
                'max'   => 100,
                'selectors' => [
                    '(mobile){{WRAPPER}} h4.product-title' => 'font-size: {{SIZE}}px',
                ],
            ]
        );

        $this->add_control(
            'category_font_size_mobile',
            [
                'label' => __('Category Font Size', 'mobimentor-slider'),
                'type' => \Elementor\Controls_Manager::NUMBER,
                'default' => 12,
                'min'   => 1,
                'max'   => 100,
                'selectors' => [
                    '(mobile){{WRAPPER}} .single-slider .slider-info .product-category' => 'font-size: {{SIZE}}px !important',
                ],
            ]
        );

        $this->add_control(
            'price_font_size_mobile',
            [
                'label' => __('Price Font Size', 'mobimentor-slider'),
                'type' => \Elementor\Controls_Manager::NUMBER,
                'default' => 12,
                'min'   => 1,
                'max'   => 100,
                'selectors' => [
                    '(mobile){{WRAPPER}} .single-slider .slider-info .product-price' => 'font-size: calc( 50% / {{SIZE}} )px !important',
                ],
            ]
        );

        $this->add_control(
            'badges_font_size_mobile',
            [
                'label' => __('Badges Font Size', 'mobimentor-slider'),
                'type' => \Elementor\Controls_Manager::NUMBER,
                'default' => 14,
                'min'   => 1,
                'max'   => 100,
                'selectors' => [
                    '(mobile){{WRAPPER}} .single-slider .slide-top-left .on-sale' => 'font-size: {{SIZE}}px !important',
                    '(mobile){{WRAPPER}} .single-slider .slide-top-left .featured' => 'font-size: {{SIZE}}px !important',
                ],
            ]
        ); */

        $this->add_control(
            'all_font_size_mobile',
            [
                'label' => __('Badges Font Size', 'mobimentor-slider'),
                'type' => \Elementor\Controls_Manager::NUMBER,
                'default' => 24,
                'min'   => 1,
                'max'   => 100,
                'selectors' => [
                    '(mobile){{WRAPPER}} h4.product-title' => 'font-size: {{SIZE}}px !important',
                    '(mobile){{WRAPPER}} .single-slider .slider-info .product-price' => 'font-size: calc( {{SIZE}}px * 0.8 ) !important',
                    '(mobile){{WRAPPER}} .single-slider .slider-info .product-category' => 'font-size: calc( {{SIZE}}px * 0.5 ) !important',
                    '(mobile){{WRAPPER}} .single-slider .slide-top-left .on-sale' => 'font-size: calc( {{SIZE}}px * 0.6 ) !important',
                    '(mobile){{WRAPPER}} .single-slider .slide-top-left .featured' => 'font-size: calc( {{SIZE}}px * 0.6 ) !important',
                ],
            ]
        );

        $this->end_controls_section();

        /* Style Section Starts */

        $this->start_controls_section(
            'title_style_section',
            [
                'label' => esc_html('Title Style', 'mobimentor-slider'),
                'tab' => \Elementor\Controls_Manager::TAB_STYLE,
                'conditions' => [
                    'relation' => 'or',
                    'terms' => [
                        [
                            'name' => 'display_title',
                            'operator' => '==',
                            'value' => 'yes'
                        ],
                        [
                            'name' => 'display_subtitle',
                            'operator' => '==',
                            'value' => 'yes'
                        ],
                        [
                            'name' => 'show_more',
                            'operator' => '==',
                            'value' => 'yes'
                        ]
                    ]
                ]
            ]
        );

        $this->add_control(
            'title_color',
            [
                'label' => __('Title Color', 'mobimentor-slider'),
                'type' => \Elementor\Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} h2.mobimentor-title' => 'color: {{VALUE}}',
                ],
                'condition' => [
                    'display_title'    =>    'yes',
                ]
            ]
        );

        $this->add_control(
            'subtitle_color',
            [
                'label' => __('Subtitle Color', 'mobimentor-slider'),
                'type' => \Elementor\Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} h3.mobimentor-subtitle' => 'color: {{VALUE}}',
                ],
                'condition' => [
                    'display_subtitle'    =>    'yes',
                ]
            ]
        );

        $this->start_controls_tabs(
            'title_link_tabs',
            [
                'condition' => [
                    'show_more'    =>    'yes',
                ]
            ]
        );

        $this->start_controls_tab(
            'title_link_normal_tab',
            [
                'label' => __('Normal', 'mobimentor-slider'),
            ]
        );

        $this->add_control(
            'show_more_link_color',
            [
                'label' => __('Show more color', 'mobimentor-slider'),
                'type' => \Elementor\Controls_Manager::COLOR,
                'separator' => 'after',
                'selectors' => [
                    '{{WRAPPER}} a.mobimentor-showmore' => 'color: {{VALUE}}',
                ],
            ]
        );

        $this->end_controls_tab();

        $this->start_controls_tab(
            'title_link_hover_tab',
            [
                'label' => __('Hover', 'mobimentor-slider'),
            ]
        );
        $this->add_control(
            'title_hover_color',
            [
                'label' => __('Hover Color', 'mobimentor-slider'),
                'type' => \Elementor\Controls_Manager::COLOR,
                'separator' => 'after',
                'selectors' => [
                    '{{WRAPPER}} a.mobimentor-showmore:hover' => 'color: {{VALUE}}',
                ],
            ]
        );
        $this->end_controls_tab();
        $this->end_controls_tabs();
        $this->end_controls_section();

        /* Title Style Ends */

        /* Product Info Style Starts */

        $this->start_controls_section(
            'product_info_color',
            [
                'label' => esc_html('Product Info Style', 'mobimentor-slider'),
                'tab' => \Elementor\Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'product_title_color',
            [
                'label' => __('Product Title Color', 'mobimentor-slider'),
                'type' => \Elementor\Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .slider-info .product-title' => 'color: {{VALUE}};'
                ]
            ]
        );

        $this->add_control(
            'product_title_hover_color',
            [
                'label' => __('Product Title Hover Color', 'mobimentor-slider'),
                'type' => \Elementor\Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .slider-info .product-title:hover' => 'color: {{VALUE}};'
                ]
            ]
        );

        $this->add_control(
            'product_price_color',
            [
                'label' => __('Product Price Color', 'mobimentor-slider'),
                'type' => \Elementor\Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .slider-info .product-price' => 'color: {{VALUE}};'
                ]
            ]
        );

        $this->add_control(
            'product_cat_color',
            [
                'label' => __('Product Category Color', 'mobimentor'),
                'type' => \Elementor\Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .slider-info .product-category' => 'color: {{VALUE}};'
                ]
            ]
        );

        $this->add_control(
            'product_cat_back_color',
            [
                'label' => __('Product Category Background Color', 'mobimentor-slider'),
                'type' => \Elementor\Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .slider-info .product-category' => 'background-color: {{VALUE}};'
                ]
            ]
        );

        /* $this->add_control(
            'content_ratio_two',
            [
                'label' => __('Content Ratio', 'plugin-domain'),
                'type' => \Elementor\Controls_Manager::SLIDER,
                'size_units' => ['%'],
                'range' => [
                    '%' => [
                        'min' => 10,
                        'max' => 30,
                    ],
                ],
                'default' => [
                    'unit' => 'px',
                    'size' => 12,
                ],
                'selectors' => [
                    '{{WRAPPER}} .product-title' => 'font-size: {{SIZE}}px;',
                ]
            ]
        ); */
        $this->end_controls_section();

        /* Product Info Styles Ends */

        /* Badges and Cart styles */

        $this->start_controls_section(
            'product_badges_style',
            [
                'label' => esc_html('Badges Styles', 'mobimentor-slider'),
                'tab' => \Elementor\Controls_Manager::TAB_STYLE,
            ]
        );

        $this->start_controls_tabs(
            'badges_style_tabs'
        );

        $this->start_controls_tab(
            'badges_style_color_tav',
            [
                'label' => __('Text Color', 'mobimentor-slider'),
            ]
        );

        $this->add_control(
            'on_sale_color',
            [
                'label' => __('On-sale Text Color', 'mobimentor-slider'),
                'type' => \Elementor\Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} span.on-sale' => 'color: {{VALUE}}',
                ],
            ]
        );

        $this->add_control(
            'featured_badge_color',
            [
                'label' => __('Featured Text Color', 'mobimentor-slider'),
                'type' => \Elementor\Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} span.featured' => 'color: {{VALUE}}',
                ],
            ]
        );

        $this->add_control(
            'cart_button_color',
            [
                'label' => __('Cart Icon Color', 'mobimentor-slider'),
                'type' => \Elementor\Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .mm-add-to-cart-btn .fa-cart-plus' => 'color: {{VALUE}}',
                    '{{WRAPPER}} .added_to_cart' => 'color: {{VALUE}}',
                ],
            ]
        );

        $this->end_controls_tab();

        $this->start_controls_tab(
            'badges_style_bgcolor_tav',
            [
                'label' => __('Background Color', 'mobimentor-slider'),
            ]
        );

        $this->add_control(
            'on_sale_bg_color',
            [
                'label' => __('On-sale Background Color', 'mobimentor-slider'),
                'type' => \Elementor\Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} span.on-sale' => 'background-color: {{VALUE}}',
                ],
            ]
        );

        $this->add_control(
            'featured_badge_bg_color',
            [
                'label' => __('Featured Background Color', 'mobimentor-slider'),
                'type' => \Elementor\Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} span.featured' => 'background-color: {{VALUE}}',
                ],
            ]
        );

        $this->add_control(
            'cart_bg_color',
            [
                'label' => __('Cart Background Color', 'mobimentor-slider'),
                'type' => \Elementor\Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .mm-add-to-cart-btn .fa-circle' => 'color: {{VALUE}}',
                    '{{WRAPPER}} .added_to_cart' => 'background-color: {{VALUE}}',
                ],
            ]
        );

        $this->end_controls_tab();
        $this->end_controls_tabs();

        /* $this->add_group_control(
            \Elementor\Group_Control_Box_Shadow::get_type(),
            [
                'name' => 'addtocart_box_shadow',
                'label' => __('Add to Cart Box Shadow', 'mobimentor'),
                'separator' => 'before',
                'selector' => '{{WRAPPER}} .mm-add-to-cart-btn:hover, {{WRAPPER}} .added_to_cart:hover',
            ]
        ); */

        $this->end_controls_section();


        /* Badges and Cart styles */

        /* Background Styles */

        $this->start_controls_section(
            'product_background_style',
            [
                'label' => esc_html('Slide Background Styles', 'mobimentor-slider'),
                'tab' => \Elementor\Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'display_background_blur',
            [
                'label' => esc_html__('Blurry Background', 'mobimentor-slider'),
                'type' => \Elementor\Controls_Manager::SWITCHER,
                'label_on' => esc_html__('Yes', 'mobimentor-slider'),
                'label_off' => esc_html__('No', 'mobimentor-slider'),
                'return_value' => 'yes',
                'default' => 'yes',
            ]
        );

        $this->add_group_control(
            \Elementor\Group_Control_Box_Shadow::get_type(),
            [
                'name' => 'slide_box_shadow',
                'label' => __('Slide Box Shadow', 'mobimentor-slider'),
                'selector' => '{{WRAPPER}} .single-slider:hover',
            ]
        );

        $this->end_controls_section();

        /* Background Styles */

        /* Style Section Ends */
    }

    /**
     * Render WooCommerce product carousel widget output on the frontend.
     *
     * Written in PHP and used to generate the final HTML.
     *
     * @since 1.0.0
     * @access protected
     */
    protected function render()
    {

        $settings = $this->get_settings_for_display();
        $slider_id = 'slider-' . $this->get_id();
        $args = mobimentor_get_slider_query($settings);

        $products = new WP_Query($args);
        /* var_dump($products->post_count); */
        $title_style = $settings['title_style'] ? $settings['title_style'] : 'title-one';
        $content_ratio = $settings['content_ratio']['size'] ? $settings['content_ratio']['size'] : 50;
        $label_size = ceil((10 / 50) * $content_ratio);

        $slider_settings = mobimentor_get_carousel_settings($settings);
?>
        <style>
            <?php echo '.' . $slider_id . ' '; ?>.added_to_cart {
                font-size: <?php echo $label_size . 'px'; ?> !important;
            }
        </style>

        <div class="mobimentor-carousel-widget <?php echo $slider_id; ?>">
            <?php
            if ($title_style == 'title-one') {
                require(MMS_IS_PATH . '/templates/header/header_one.php');
            } else {
                require(MMS_IS_PATH . '/templates/header/header_one.php');
            }
            ?>
            <div class="mobimentor-slick-slider mobimentor-slick-slider-one">
                <?php
                if ($products->have_posts()) :
                    while ($products->have_posts()) : $products->the_post();
                        require(MMS_IS_PATH . '/templates/carousel/carousel_two.php');
                    endwhile;
                    wp_reset_postdata();
                endif;
                ?>
            </div>
            <input type="hidden" class="slider-settings" value='<?php echo json_encode($slider_settings); ?>' />
        </div>
<?php
    }
}
